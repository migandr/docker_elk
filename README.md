# elk_docker

This repository contains the first version for the elast stack, to execute you have to clone this repository and execute:

```bash
  docker-compose up -d
```

### Folders

You will find a folder with the specific configuration for each element (elasticsearch, kibana and logstash).

Kibana and logstash have a field in their `.yml` file for the password, the password for elasticsearch is configured directly in
the docker-compose file.

The password that is set up now is **1234qwert**, of course, this is a temporary password.
